unit fmVideo;

interface
{$I cef.inc}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, RunText, jpeg, ExtCtrls, ADODB, DB,Math,
  AdvGlassButton, Gradient, RXCtrls, StrUtils,ComObj,
  StdCtrls, AdvGDIPicture,AdvPicture, RXClock, tscap32_rt,
  ShellApi, HotKeyManager, PasLibVlcPlayerUnit,INIfiles, OleCtrls, SHDocVw,
  cefvcl,ceflib;

type
  TfrmVideo = class(TForm)
    splitNews: TSplitter;
    RxLabel2: TRxLabel;
    pnlUp: TPanel;
    splitAds: TSplitter;
    pnlMovie: TPanel;
    pnlAds: TPanel;
    pnlNews: TPanel;
    RunningText1: TRunningText;
    Splitter1: TSplitter;
    Timer1: TTimer;
    TimerB: TTimer;
    pnlTest: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    pnlClock: TPanel;
    RxClock1: TRxClock;
    tsCap32_Show: TtsCap32;
    tsCap32PopupMenu1: TtsCap32PopupMenu;
    tsCap32Dialogs1: TtsCap32Dialogs;
    Image01: TAdvGDIPPicture;
    Image02: TAdvGDIPPicture;
    vlcPlay01: TPasLibVlcPlayer;
    vlcList01: TPasLibVlcMediaList;
    Image03: TAdvGDIPPicture;
    KeyHook: THotKeyManager;
    timLog: TTimer;
    timWebPage: TTimer;
    Chrome: TChromium;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TimerBTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure vlcPlay01MediaPlayerEndReached(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure KeyHookHotKeyPressed(HotKey: Cardinal; Index: Word);
    procedure FormHide(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure timLogTimer(Sender: TObject);
    procedure timWebPageTimer(Sender: TObject);
    procedure WebBrowser1NavigateComplete2(Sender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure ChromeLoadStart(Sender: TObject; const browser: ICefBrowser;
      const frame: ICefFrame);
    procedure ChromeLoadEnd(Sender: TObject; const browser: ICefBrowser;
      const frame: ICefFrame; httpStatusCode: Integer;
      out Result: Boolean);
  private
    { Private declarations }
    FLoading: Boolean;
    procedure DBconnect;
    procedure SetScreen;
    procedure PlayVideo(VName: string);
    function NextAvailVideo: string;
    function LoadNews: string;
    function AtLeastVideoAvail: boolean;
    function FirstVideoAvail: boolean;

    function NextAvailBanner1: string;
    function NextAvailBanner2: string;

    procedure ToClose;
    procedure ShowDefaultDevice;
    procedure ShowDefaultStatus;
    function CompactAndRepair: Boolean;
    procedure CompactDB;
  public
    { Public declarations }
    procedure RefreshPlayQ;
    procedure RefreshNews;
    procedure RefreshBanner;
    function CheckFileExist(FName: string): boolean;
    procedure ShowBanners;

    procedure UpdateConfigVariable;
    procedure UpdateScreen;
    procedure TrackLog(sCategory: string; sValue:string);
    procedure SaveLog(sCategory: string; sValue:string);
    procedure ReadConfig();
    procedure UpdateFullScreen();
    procedure DBLogPlay(sQueue,sType,sFile,sAction:string);
    function CheckBannerQueue(myDB:TADOTable):boolean;
    function NextBanner1():string;
    function NextBanner2():string;
    procedure PlayStream(sURL: string);
    procedure AddList(vlcList: TPasLibVlcMediaList; strFile,sType: string);
    procedure LoadURL(browser:Tchromium;sURL:string);
    procedure SetBrowserPos(oVideo:TPasLibVlcPlayer;oBrowser:TChromium);
    procedure RefreshHTML();
    function NextHTML():string;
  end;

  TCefStreamDownloadHandler = class(TCefDownloadHandlerOwn)
  private
    FStream: TStream;
    FOwned: Boolean;
  protected
    function ReceivedData(data: Pointer; DataSize: Integer): Integer; override;
    procedure Complete; override;
  public
    constructor Create(stream: TStream; Owned: Boolean); reintroduce;
    destructor Destroy; override;
  end;

const
  cProduct='InfoExpress-Lite';
  cRegVersion='Player';
  cVersion='1.4.0';
var
  frmVideo: TfrmVideo;

  CurrVideoIdx: integer; // Keep Current Video Index
  NextVideoName: string; // Keep Next Video Name

  iMaxBanner1,iMaxBanner2:integer; // Keep Max Each Banner List
  iMaxVideo: integer;  // Keep Max Video List

  CurrBannerIdx1, CurrBannerIdx2: integer;  // Keep Current Banner Index
  NextBannerName1, NextBannerName2: string;
  CurrBannerName1, CurrBannerName2: string;
  sCurrVideoFile: string; // Keep Current Video Play

  Client_ID: integer; // Keep data from database
  Terminal_ID: integer; // Keep data from database
  Show_News: Boolean; // Keep data from database
  Show_Ads: Boolean; // Keep data from database
  Video_Dir: string; // Keep data from database
  Def_video: string; // Keep data from database
  http_server: string; // Keep data from database
  video_program: string; // Keep data from database
  news_program: string; // Keep data from database
  refresh_time: integer; // Keep data from database

  debug_mode: boolean; // Keep data from database
//Start add by Boripat
  default_videoin: boolean; // Keep data from database
  def_type:integer; // Keep data from database
  def_file:string; // Keep data from database
  bDefaultPlayer: boolean; // Keep Now Playing Default ?
  bStreaming: boolean; // Keep Now Playing Streaming ?

  //End End
  FullVideoDir: string; // Keep Video Folder
  bVideoRatio: Boolean; // Keep Data from INI to config to play video in KeepRatio?
  sProgramDir: string; // Keep Current Program Directory
  IsExiting: boolean;  // Keep Now is Exiting Mode

  BannerInterval: integer; // Keep data from database
  BannerMode: integer; // Keep data from database

  NewsStyles: TFontStyles; // Keep data from database
  wHandle: THandle;
  myLogFile: TextFile;
  bDebugPlayer: Boolean; // Keep data from INI track process?
  bTrackLog: Boolean; // Keep data from INI track process?
  sTrackLog: string; // Keep data from INI track log file name
  sOnlineLog: String; // Keep data from INI online log file name
  iIntervalLog:integer; // Keep data from INI online log file name
  bShowHTML: Boolean; // Keep data from INI
  iHTMLSize:integer;
  iRunHTML:integer;
  iMaxHTML:integer;
  iIntervalHTML:integer;
  sHTMLDirection: string;
  arrURL  : Array of string;
//  function IsRegistered: integer; stdcall; external 'dslic.dll';

  lastVideoName: string;
  iOldLeft,iOldTop,iOldWidth,iOldHeight: integer; // Keep Old Placement Form
  iNewLeft,iNewTop,iNewWidth,iNewHeight: integer; // Keep New Placement Form
  sNewRatio,sOldRatio: string; // Keep Old and New Video Ratio
  bFullScreen: boolean; // Keep Full Screen Mode

  iLoopVideo:integer;  // Keep Loop Count From Start Program
  iLoopBanner1:integer;// Keep Loop Count From Start Program
  iLoopBanner2:integer;// Keep Loop Count From Start Program
  sVideoType: string; // Keep Video Type on NextAvailVideo

  sRegName01,sRegName02: string; // Keep Register Name
  pData01,pData02: pChar;
  sTmp01,sTmp02: string;

  sError:string;

  iCurrQueue,iLastQueue: integer;  // Keep Last and Current Queue ID
  procedure dll_GetRegistry(const strPName: PChar; const strKey: PChar;Out strOut: PChar); external 'RevenueExpress.dll';
  function dll_AddRegistry(const strPName: PChar;const strKey: pChar; const strValue: pChar): boolean; external 'RevenueExpress.dll';
  procedure FreePChar(p: Pchar); external 'RevenueExpress.dll';


implementation

uses
  data,fmPlacement,ceffilescheme;

{$R *.dfm}

function getpath(const n: ICefDomNode): string;
begin
  Result := '<' + n.Name + '>';
  if (n.Parent <> nil) then
    Result := getpath(n.Parent) + Result;
end;

function FileInUse(FileName: string): Boolean;
var hFileRes: HFILE;
begin
  Result := False;
  if not FileExists(FileName) then exit;
  hFileRes := CreateFile(PChar(FileName),
                                    GENERIC_READ or GENERIC_WRITE,
                                    0,
                                    nil,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL,
                                    0);
  Result := (hFileRes = INVALID_HANDLE_VALUE);
  if not Result then
    CloseHandle(hFileRes);
end;

{ TCefStreamDownloadHandler }

procedure TCefStreamDownloadHandler.Complete;
begin
//  MainForm.StatusBar.SimpleText := 'Download complete';
end;

constructor TCefStreamDownloadHandler.Create(stream: TStream; Owned: Boolean);
begin
  inherited Create;
  FStream := stream;
  FOwned := Owned;
end;

destructor TCefStreamDownloadHandler.Destroy;
begin
  if FOwned then
    FStream.Free;
  inherited;
end;

function TCefStreamDownloadHandler.ReceivedData(data: Pointer;
  DataSize: Integer): Integer;
begin
  Result := FStream.Write(data^, DataSize);
//  MainForm.StatusBar.SimpleText := 'Downloading ... ' + IntToStr(FStream.Position div 1000) + ' Kb';
end;



procedure TfrmVideo.FormCreate(Sender: TObject);
var
  userResponse: integer;
  sDisplay: string;
  AppHandle:THandle;
  iniFile: TIniFile;
begin

  dll_AddRegistry(pchar(cProduct),pchar(cRegVersion),pchar(cVersion));

  Self.Width:=Screen.Width; // Define Form to Full Screen
  Self.Height:=screen.Height; // Define From to Full Screen
  default_videoin:=false; // Define Default Value
  bDefaultPlayer:=false;  // Define Default Value
  bStreaming:=false;  // Define Default Value
//  if IsRegistered = 0 then
//  begin
//    MessageDlg('Software was not registered. Please contact your vendor.', mtError, [mbok], 0);
//    Application.Terminate;
//  end;
  KeyHook.AddHotKey(vk_f11); // Define HotKey F10
  bVideoRatio:=false; // Define Default Value
  bTrackLog:=false; // Define Default Value
  // force to close video agent
{
  AppHandle := FindWindow(nil, 'DS Agent');

  if AppHandle <> 0 then
    PostMessage(AppHandle, WM_QUIT, 0, 0);
  //--------------------------------------------

}
  sProgramDir:=ExtractFilePath(Application.ExeName); // Get Current Directory
  if (not DirectoryExists(sProgramDir+'log\')) then begin
    CreateDir(sProgramDir+'log\');
  end;
  ReadConfig(); // Read INI Config File

  timLog.Interval:=1000*iIntervalLog; // Set Interval for Online Record
  timLog.Enabled:=true; // Start Timer for Record Online Log
  Application.Title:='DS Player '+cVersion;
  FLoading := False;
  iIntervalHTML:=30;
end;

procedure TfrmVideo.ReadConfig();
var
  iniFile: TIniFile;
  iLoop:integer;
  sURL:string;
begin
  iOldLeft:=frmVideo.Left;
  iOldTop:=frmVideo.Top;
  iOldWidth:=frmVideo.Width;
  iOldHeight:=frmVideo.Height;
  sOldRatio:=vlcPlay01.GetAspectRatio;
  iniFile:=TiniFile.Create(sProgramDir+'display.ini');
  sTrackLog:='track_smp';
  try
    iNewLeft:=iniFile.ReadInteger('Placement','left',0);
    iNewTop:=iniFile.ReadInteger('Placement','top',0);
    iNewWidth:=iniFile.ReadInteger('Placement','width',640);
    iNewHeight:=iniFile.ReadInteger('Placement','height',480);
    bFullScreen:=(1=iniFile.ReadInteger('Placement','fullscreen',0));
    bVideoRatio:=(1=iniFile.ReadInteger('video','ratio',1));

    if (bFullScreen) then begin
      self.Left:=-1;
      self.Top:=-1;
      self.Width:=screen.Width+2;
      self.Height:=screen.Height+2;
      self.ClientHeight:=screen.Height+2;
      self.ClientWidth:=screen.Width+2;
    end else begin
      self.Left:=iNewLeft;
      self.Top:=iNewTop;
      self.Width:=iNewWidth;
      self.Height:=iNewHeight;
      self.ClientHeight:=iNewHeight;
      self.ClientWidth:=iNewWidth;
    end;

  except
    on E : Exception do begin
      sError:='Exception on Function = ReadConfig # display.ini';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
  iniFile.Free;

  iniFile:=TiniFile.Create(sProgramDir+'config.ini');
  try
    bTrackLog:=(1=iniFile.ReadInteger('track','player',1));
    bDebugPlayer:=(1=iniFile.ReadInteger('debug','player',1));
    sOnlineLog:=iniFile.ReadString('online_log','player','online_smp');
    iIntervalLog:=iniFile.ReadInteger('online_status','interval',1);
    sTrackLog:=iniFile.ReadString('track_log','player','track_smp');
    //    iTmp:=iniFile.ReadInteger('Video','ratio',1);
//      bVideoRatio:=(iTmp = 1);
  except
    on E : Exception do begin
      sError:='Exception on Function = ReadConfig # config.ini';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
  iniFile.Free;
{
  iniFile:=TiniFile.Create(sProgramDir+'url.ini');
  try
    bShowHTML:=(1=iniFile.ReadInteger('main','active',1));
    iIntervalHTML:=iniFile.ReadInteger('main','interval',60);
    iMaxHTML:=iniFile.ReadInteger('main','items',0);
    sHTMLDirection:=iniFile.ReadString('main','direction','left');
    iHTMLSize:=iniFile.ReadInteger('main','size',50);
    setLength(arrURL,iMaxHTML);
    for iLoop:=1 to iMaxHTML do begin
      sURL:=iniFile.ReadString('url','url'+inttostr(iLoop),'http://www.signageexpress.com');
      arrURL[iLoop-1]:=sURL;
    end;
  except
    on E : Exception do begin
      sError:='Exception on Function = ReadConfig # url.ini';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
  iniFile.Free;
}
end;

procedure TfrmVideo.ToClose;
var
  AppHandle:THandle;
begin
  IsExiting := True;

  // force to close video agent
{
  AppHandle := FindWindow(nil, 'DS Agent');

  if AppHandle <> 0 then
    PostMessage(AppHandle, WM_QUIT, 0, 0);
  //--------------------------------------------

//  Close;
}
end;

//--------------------------------------------------------------------------------
// Main Timer Functions
//--------------------------------------------------------------------------------
procedure TfrmVideo.Timer1Timer(Sender: TObject);
var
  sDisplay:string;
  rKey:integer;
  tmcur: integer;
  tmnext: integer;
  iTmp: integer;
  sDate: string; // Keep Date
begin
  try
    if not IsExiting then begin

      if refresh_time > 0 then begin
        sDate:=formatdatetime('hh:nn:ss',now);
        tmcur:=strtoint(copy(sDate,7,2));
        tmnext:=(refresh_time * 60 * 1000);
        if (tmcur <> 0) then
          tmnext:=tmnext - (tmcur * 1000);
          Timer1.Interval := tmnext;
          Timer1.Enabled:=true;
      //    Timer1.Interval := (refresh_time * 60 * 1000);

      end else
        Timer1.Interval := 1200000;          // default = 20 minutes.

      RefreshPlayQ;
      if (iLastQueue <> iCurrQueue) then begin
        SetScreen;
        sDisplay:='change queue from '+inttostr(iLastQueue)
          + ' to ' + inttostr(iCurrQueue);
        TrackLog('video',sDisplay);
        sDisplay:=inttostr(iCurrQueue)+'/'+inttostr(iLastQueue);
        DBLogPlay('0','Queue',sDisplay,'change');
        iLastQueue:=iCurrQueue;
        currVideoIdx:=0;
        vlcList01.Clear;
        vlcList01.Stop;
        TimerB.Enabled:=false;
        TimerB.Enabled:=true;
      end;
      //      RefreshNews;
        if Show_News then RefreshNews;

        if Show_Ads then RefreshBanner;

      ShowDefaultDevice;
      SetScreen;
{
      sDisplay:=' Movie Area => ' + inttostr(pnlMovie.Width)
        + ' X ' + inttostr(pnlMovie.Height);
      TrackLog('ratio','[Interval] '+sDisplay);
      sDisplay:= ' VLC Area => ' + inttostr(vlcPlay01.Width)
        + ' X ' + inttostr(vlcPlay01.Height);
      TrackLog('ratio','[Interval] '+sDisplay);
      sDisplay:=' Current Ratio => '+vlcPlay01.GetAspectRatio;
      TrackLog('ratio','[Interval] '+sDisplay);
      sDisplay:=inttostr(vlcPlay01.Width)+':'+inttostr(vlcPlay01.Height);
      if (bVideoRatio) then begin
        vlcPlay01.SetAspectRatio('');
        TrackLog('ratio','[Internal] Set VLC KeepRatio');
      end else begin
        vlcPlay01.SetAspectRatio(sDisplay);
        TrackLog('ratio','[Internal] Set VLC Ratio ::'+sDisplay);
      end;
}
    end;
  except

  end;
end;

procedure TfrmVideo.SetScreen;
var
  tmph:integer;
  iImgWidth:integer;
  tblData: TADOTable;
  sError:string;
begin
  if (bDebugPlayer) then
    MessageDlg('Set Screen',mtInformation,[mbYes],0);
  sOldRatio:=vlcPlay01.GetAspectRatio;
  ReadConfig();

  tblData:=dm.tblPlayLayout;
  if (dm.tblPlayLayout.IsEmpty) then
    tblData:=dm.tblConfig;

  with tblData do begin
    if active then close;
    open;
    UpdateConfigVariable;
    First;
    if (iNewWidth <> iOldWidth) and (iNewHeight <> iNewHeight)
      and (iNewLeft <> iOldLeft) and (iNewTop <> iNewTop) then begin
      self.Width:=iNewWidth;
      self.Height:=iNewHeight;
      self.Left:=iNewLeft;
      self.Top:=iNewTop;
      self.ClientHeight:=iNewHeight;
      self.ClientWidth:=iNewWidth;
    end;

    if (bFullScreen) then begin
      self.Left:=-1;
      self.Top:=-1;
      self.Width:=screen.Width+2;
      self.Height:=screen.Height+2;
      self.ClientHeight:=screen.Height+2;
      self.ClientWidth:=screen.Width+2;
    end;
    if Show_News then begin
      pnlNews.Height := 65;
      pnlNews.Height := trunc(ClientHeight * FieldbyName('news_height').AsInteger / 100);
      Image03.Width := trunc(Screen.Width * 0.1);

      pnlClock.Width := 170;
    end else begin
      pnlNews.Height := 0;
    end;

    if Show_Ads then begin
      pnlAds.Width := trunc(ClientWidth * FieldbyName('banner_width').AsInteger / 100);    //  default is 208

      if FieldbyName('banner_prop').AsBoolean then begin
        Image01.Proportional:=true;
        Image02.Proportional:=true;
        Image01.Stretch:=false;
        Image02.Stretch:=false;
//        Image01.PicturePosition := bpStretchedWithAspect;
//        Image02.PicturePosition := bpStretchedWithAspect;
      end else begin
        Image01.Proportional:=false;
        Image02.Proportional:=false;
        Image01.Stretch:=true;
        Image02.Stretch:=true;
//        Image1.PicturePosition := bpStretched;
//        Image2.PicturePosition := bpStretched;
      end;

      if BannerMode = 1 then begin
        Image01.Height := pnlAds.Height;
        Image02.Height := 0;
      end else begin
        tmph := pnlAds.Height div 2;
        Image01.Height := tmph;
        Image02.Height := tmph;
      end;
    end else begin
      pnlAds.Width := 0;
    end;

    RunningText1.Left:=image03.Width+1;
    RunningText1.Font.Name := FieldByName('news_font').AsString;
//      RunningText1.Font.Size := FieldByName('news_size').AsInteger;
    RunningText1.Font.Size := floor(pnlNews.Height/2);
    RunningText1.Font.Color := FieldByName('news_color').AsInteger;
    RunningText1.Color := FieldByName('news_back_color').AsInteger;

    RxClock1.Font.Name := FieldByName('news_font').AsString;
    RxClock1.Color := FieldByName('news_back_color').AsInteger;
    RxClock1.Font.Color := FieldByName('news_color').AsInteger;

    if FieldByName('news_delay').AsInteger > 0 then
      RunningText1.Speed := FieldByName('news_delay').AsInteger
    else
//        RunningText1.Speed := 50;
        RunningText1.Speed := 200;

    if FieldByName('news_step').AsInteger > 0 then begin
      if RunningText1.steps <> FieldByName('news_step').AsInteger then begin
        RunningText1.Active := false;
        RunningText1.steps := FieldByName('news_step').AsInteger;

        sleep(1000);
        RunningText1.Active := true;
      end;
    end else
//        RunningText1.steps := 50;
        RunningText1.steps := 200;

    NewsStyles := [];
    if FieldByName('news_isbold').AsInteger <> 0 then
      Include(NewsStyles, fsBold);
    if FieldByName('news_isitalic').AsInteger <> 0 then
      Include(NewsStyles, fsItalic);
    if FieldByName('news_isunderline').AsInteger <> 0 then
      Include(NewsStyles, fsUnderline);
    RunningText1.Font.Style := NewsStyles;

    if not RunningText1.Active then RunningText1.Active := true;

    if FieldByName('show_clock').AsBoolean then begin
//        pnlclock.Width := 165;
      pnlClock.Width := floor(3.8*RunningText1.Height);
      RxClock1.Font.Size := floor(pnlclock.Height/2);
    end else
        pnlclock.Width := 0;
// Change Logo Image Size
    image03.Width:=FieldByName('logo_width').AsInteger;
    iImgWidth:=FieldByName('logo_width').AsInteger;
    if (iImgWidth < 100) then
      image03.Width:=floor(ClientWidth*iImgWidth/100);
    image03.Stretch:=fieldbyname('logo_stretch').AsBoolean;
    image03.Proportional:=fieldbyname('logo_ratio').AsBoolean;
//
  end;

//  sNewRatio:=inttostr(pnlMovie.Width)+':'+inttostr(pnlMovie.height);
{
  vlcPlay01.Align:=alClient;
  sNewRatio:=inttostr(vlcPlay01.Width)+':'+inttostr(vlcPlay01.height);
  if (sNewRatio <> sOldRatio) then begin
    if (bVideoRatio) then begin
      TrackLog('Video','Display:: Keep Ratio');
      vlcPlay01.SetAspectRatio('');
      vlcPlay01.Repaint;
    end Else begin
      TrackLog('Video','Set Last Display:: '+vlcPlay01.GetAspectRatio);
      vlcPlay01.SetAspectRatio(sNewRatio);
      TrackLog('Video','Set Display::'+sNewRatio+' Change=>'+vlcPlay01.GetAspectRatio);
      vlcPlay01.Repaint;
    end;
  end;
}
{
  if refresh_time > 0 then
    begin
    sDate:=formatdatetime('hh:nn:ss',now);
    tmcur:=strtoint(copy(sDate,7,2));
    tmnext:=(refresh_time * 60 * 1000);
    if (tmcur <> 0) then
      tmnext:=tmnext - (tmcur * 1000);
    Timer1.Interval := tmnext;
//    Timer1.Interval := (refresh_time * 60 * 1000);

    end
  else
    Timer1.Interval := 1200000;          // default = 20 minutes.

  Timer1.Enabled := True;
}
  if Show_Ads then begin
    if BannerInterval > 0 then
      Timerb.Interval := BannerInterval * 1000
    else
      Timerb.Interval := 5000;
    Timerb.Enabled := True;
  end else begin
    TimerB.Enabled := False;
  end;

  with vlcPlay01 do begin
    Left:=0;
    Top:=0;
    Height:=pnlMovie.Height;
    Width:=pnlMovie.Width;
  end;

  if( not (bShowHTML and (iMaxHTML>0))) then begin
    Chrome.Visible:=false;
  end else begin
//    SetBrowserPos(vlcPlay01,Chrome);
{
    timWebPage.Enabled:=false;
    timWebPage.Interval:=iIntervalHTML*1000;
    timWebPage.Enabled:=true;
    try
      TrackLog('Browser','[SetScreen] Show URL : '+arrURL[iRunHTML]);
      WebBrowser1.Navigate(arrURL[iRunHTML]);
    except
      on E : Exception do begin
        sError:='Exception on Function = WebPage, '+arrURL[iRunHTML]+') ';
        sError:=sError+#13#10+'Exception Class = '+E.ClassName;
        sError:=sError+#13#10+'Exception Message = '+E.Message;
        ShowMessage(sError);
      end;
    end;
}
    Chrome.Visible:=true;
    if (sHTMLDirection='top') then begin  // Top
      with Chrome do begin
//      with WebBrowser1 do begin
        Left:=0;
        Top:=0;
        Height:=floor(pnlMovie.Height*iHTMLSize/100);
        Width:=pnlMovie.Width;
      end;
      with vlcPlay01 do begin
        Left:=0;
        Top:=Chrome.Height+1;
        Height:=floor(pnlMovie.Height*(100-iHTMLSize)/100);
        Width:=pnlMovie.Width;
      end;
    end else if (sHTMLDirection='bottom') then begin  // Bottom
      with vlcPlay01 do begin
        Left:=0;
        Top:=0;
        Height:=floor(pnlMovie.Height*(100-iHTMLSize)/100);
        Width:=pnlMovie.Width;
      end;
      with Chrome do begin
//      with WebBrowser1 do begin
        Left:=0;
        Top:=vlcPlay01.Height+1;
        Height:=floor(pnlMovie.Height*iHTMLSize/100);
        Width:=pnlMovie.Width;
      end;
    end else if (sHTMLDirection='left') then begin  // Left
      with Chrome do begin
//      with WebBrowser1 do begin
        Left:=0;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=floor(pnlMovie.Width*iHTMLSize/100);
      end;
      with vlcPlay01 do begin
        Left:=Chrome.Width+1;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=floor(pnlMovie.Width*(100-iHTMLSize)/100);
      end;
    end else if (sHTMLDirection='right') then begin  // Right
      with vlcPlay01 do begin
        Left:=0;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=floor(pnlMovie.Width*(100-iHTMLSize)/100);
      end;
      with Chrome do begin
//      with WebBrowser1 do begin
        Left:=vlcPlay01.Width+1;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=floor(pnlMovie.Width*iHTMLSize/100);
      end;
    end else begin  // Else
      with vlcPlay01 do begin
        SendToBack;
        Visible:=false;
      end;
      with Chrome do begin
//      with WebBrowser1 do begin
        Left:=0;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=pnlMovie.Width;
        SendToBack;
      end;
    end;
  end;
end;

procedure TfrmVideo.RefreshPlayQ;
begin
//  with dm.tblPlayQ do
  with dm.tblQueuePlay do
  begin
    if active then close;
    open;
    if not eof then begin
      First;
      iCurrQueue:=fieldbyname('id').AsInteger;
    end;
  end;

//  with dm.tblQF do
  with dm.tblPlayVideo do
  begin
    if active then close;
    open;
  end;
end;

procedure TfrmVideo.RefreshNews;
begin
  RunningText1.Caption := LoadNews;
  //RunningText1.Active := show_news;
end;

procedure TfrmVideo.RefreshBanner;
begin
//  with dm.tblPlayBq1 do
  with dm.tblPlayBanner1 do
  begin
    if active then close;
    open;
  end;

//  with dm.tblPlayBq2 do
  with dm.tblPlayBanner2 do
  begin
    if active then close;
    open;
  end;

//  with dm.tblBanner do
//  begin
//    if active then close;
//    open;
//  end;
end;

//--------------------------------------------------------------------------------
// News Loader
//--------------------------------------------------------------------------------
function TfrmVideo.LoadNews: string;
begin
  Result := '';
  if show_news then
  begin
//    with dm.tblNews do
    with dm.tblPlayNews do
    begin
      if active then close;
      open;

      first;
      while not eof do
      begin
{
        if (Now >= fieldbyname('startdate').AsDateTime) and
           (Now <= fieldbyname('stopdate').AsDateTime) and
           fieldbyname('active').AsBoolean then
}
        Result := Result + '     ' + fieldbyname('headline').AsString;
        next;
      end;
    end;
  end;
end;

//--------------------------------------------------------------------------------
// Function on start program
//--------------------------------------------------------------------------------
procedure TfrmVideo.FormShow(Sender: TObject);
var
  rkey: integer;
  sVersion: string;
begin
//  frmPlacement.Hide;
//  ShowCursor(True);
  ShowCursor(False);
  CurrVideoIdx := 0;
  NextVideoName := '';

  CurrBannerIdx1 := 0;
  CurrBannerIdx2 := 0;
  NextBannerName1 := '';
  NextBannerName2 := '';
  CurrBannerName1 := 'ban01.jpg';
  CurrBannerName2 := 'ban02.jpg';
  iRunHTML:=0;
  IsExiting := False;
  RunningText1.Width := Screen.width - 220;

//  tscap32_show.Height:=activemovie1.Height;
//  tscap32_show.Width := activemovie1.Width;
//  tscap32_show.Top := activemovie1.Top;
//  tscap32_show.Left := activemovie1.Left;

  tscap32_show.Height:=vlcPlay01.Height;
  tscap32_show.Width := vlcPlay01.Width;
  tscap32_show.Top := vlcPlay01.Top;
  tscap32_show.Left := vlcPlay01.Left;

//  activemovie1.Visible:=false;

//  CompactDB;
  DBConnect;
  FullVideoDir := ExtractFilePath(Application.ExeName) + Video_Dir;

  if FileExists( FullVideoDir + 'logo.jpg' ) then
  begin
    image03.Picture.LoadFromFile(FullVideoDir + 'logo.jpg');
  end;
//  default_videoin:=  dm.tblConfig.FieldByName('video_source').AsBoolean;
  default_videoin:= false;
  Show_News := dm.tblConfig.FieldByName('show_news').AsBoolean;
  Show_Ads := dm.tblConfig.FieldByName('show_ads').AsBoolean;

  SetScreen;
  RefreshPlayQ;
  RefreshNews;

  //if Show_News then RefreshNews;

  if Show_Ads then
  begin
    RefreshBanner;
    ShowBanners;
  end;

{
  if AtLeastVideoAvail then
  begin

    if FirstVideoAvail then
    begin
//      PlayVideo(dm.tblQF.fieldbyname('newfilename').AsString);
//      dm.tblPlayVideo.First;
 //     PlayVideo(dm.tblPlayVideo.fieldbyname('newfilename').AsString);
      NextVideoName := NextAvailVideo;
      if NextVideoName <> '' then PlayVideo(NextVideoName);
    end
    else
    begin
      NextVideoName := NextAvailVideo;
      if NextVideoName <> '' then PlayVideo(NextVideoName);
    end;
    repeat
      NextVideoName := NextAvailVideo;
    until (NextVideoName <> '');
    PlayVideo(NextVideoName);
  end
  else
  begin
  end;
}
//  showDefaultDevice;
  Timer1.Interval:=1000;
  Timer1.Enabled:=true;
  // comment vcag
{
  ShellExecute(Handle, 'open', 'DSagn.exe', nil, nil, SW_SHOWNORMAL);
}
  SaveLog(sOnlineLog,formatdatetime('yyyy-mm-dd hh:nn:ss',now));
  TrackLog('video','program start');
  TrackLog('banner','program start');
  TrackLog('debug','program start');
  DBLogPlay('0','Prog','Program Start','Start');
//  WebBrowser1.Navigate('www.mthai.com');

  RefreshHTML();
  if (bShowHTML and (iMaxHTML>0)) then begin
    timWebPage.Enabled:=false;
    timWebPage.Interval:=iIntervalHTML*1000;
    timWebPage.Enabled:=true;
    LoadURL(Chrome,arrURL[0]);
  end;

end;

procedure TfrmVideo.DBconnect;
begin
  with dm.db do
  begin
    if Connected then Connected := False;
    ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;Data Source=db.mdb;Persist Security Info=True';
    Connected := True;
  end;

  with dm do
  begin
    tblConfig.Open;
    tblNews.Open;
    tblQF.Open;
    tblPlayQ.Open;

    tblQueuePlay.Open;
    tblPlayBanner1.Open;
    tblPlayBanner2.Open;
    tblPlayVideo.Open;
    tblPlayNews.Open;
    tblPlayLayout.Open;
    tblLogPlay.open;
  end;

  UpdateConfigVariable;
end;

procedure TfrmVideo.UpdateConfigVariable;
var rKey : integer;
  tblData: TADOTable;
begin
  tblData:=dm.tblPlayLayout;
  if (dm.tblPlayLayout.IsEmpty) then
    tblData:=dm.tblConfig;

  with tblData do begin
    First;
    Client_ID := FieldByName('ClientID').AsInteger;
    Terminal_ID := FieldByName('TerminalID').AsInteger;
    Show_News := FieldByName('show_news').AsBoolean;
    Show_Ads := FieldByName('show_ads').AsBoolean;
    Video_dir := StringReplace(trim(FieldByName('video_directory').AsString),
      '/','\',[rfReplaceAll, rfIgnoreCase]);
    Def_video := trim(FieldByName('default_video').AsString);

    http_server := trim(FieldByName('http_server').AsString);
    video_program := trim(FieldByName('video_program').AsString);
    news_program := trim(FieldByName('news_program').AsString);

    refresh_time := FieldByName('refresh_time').AsInteger;
//  ftp_server := trim(FieldByName('ftp_server').AsString);
//  ftp_port := FieldByName('ftp_port').AsInteger;
//  ftp_user := trim(FieldByName('ftp_user').AsString);
//  ftp_pass := trim(FieldByName('ftp_pass').AsString);
//  ftp_path := trim(FieldByName('ftp_path').AsString);
//  ftp_passive := FieldByName('ftp_passive').AsBoolean;
    debug_mode := FieldByName('debug_mode').AsBoolean;
    default_videoin := FieldByName('video_source').AsBoolean;

    def_type:=FieldByName('def_type').AsInteger;
    def_file:=trim(FieldByName('def_file').AsString);

    BannerMode := FieldByName('banner_mode').AsInteger;
    BannerInterval := FieldByName('banner_show_interval').AsInteger;
    bShowHTML:=FieldbyName('html_show').AsBoolean;
    iHTMLSize:=FieldByName('html_size').AsInteger;
    sHTMLDirection:=lowercase(FieldbyName('html_pos').AsString);

  end;


  // Start Comment by Boripat

//      tscap32_show.Left:=vlcPlay01.Left;
//      tscap32_show.Top:=vlcPlay01.Top;
//      tscap32_show.Width:=vlcPlay01.Width;
//      tscap32_show.Height:=vlcPlay01.Height;

//      tscap32_show.Left:=activemovie1.Left;
//      tscap32_show.Top:=activemovie1.Top;
//      tscap32_show.Width:=activemovie1.Width;
//      tscap32_show.Height:=activemovie1.Height;

//    if (default_videoin) then
//    begin
//      rKey:=messagedlg('UpdateConfigVariable::default_videoin=>true',mtConfirmation ,mbOKCancel,0);
//      tscap32_show.Left:=activemovie1.Left;
//      tscap32_show.Top:=activemovie1.Top;
//      tscap32_show.Width:=activemovie1.Width;
//      tscap32_show.Height:=activemovie1.Height;
//      tsCap32_show.Connected:=true;
//      tscap32_show.Visible:=true;
//      activemovie1.Visible:=false;
//    end
//    else
//    begin
//      rKey:=messagedlg('UpdateConfigVariable::default_videoin=>false',mtConfirmation ,mbOKCancel,0);
//      tsCap32_show.Connected:=false;
//      tscap32_show.Visible:=false;
//      activemovie1.Visible:=true;
//      activemovie1.OpenSync;
//    end
// End Comment
end;

//--------------------------------------------------------------------------------
// Playing Video Manager
//--------------------------------------------------------------------------------
procedure TfrmVideo.PlayVideo(VName: string);
var
  userResponse: integer;
  sDisplay: string;
begin
  try
// Comment By Boripat
    tscap32_show.Visible:=false;
    tscap32_show.Connected:=false;

//    activemovie1.Visible:=true;
      vlcPlay01.Visible:=true;

// Start Add by Boripat
    sDisplay := Runningtext1.Caption+ '::Play Video ' + VName;
//    RunningText1.Caption:=sDisplay;
//    RunningText1.Refresh;
//    userResponse:=messagedlg(sDisplay,mtConfirmation ,mbOKCancel,0);
//    userResponse:=messagedlg(sDisplay,mtinformation,mbYesNoCancel,0);
//    case userResponse of
//      idYes: default_videoin:=true;
//      idNo: default_videoin:=false;
//      idCancel: default_videoin:=false;
//    end;
//

//    if ActiveMovie1.OpenState <> nsClosed then
//    begin
//      ActiveMovie1.Close;
//    end;
    if (vlcPlay01.isPlay) then
      begin
        vlcList01.Stop;
      end;
    vlcList01.Clear;

    if LowerCase(LeftStr(VName, 7)) <> 'http://' then
    begin
      if CheckFileExist(VName) then
      begin
//        ActiveMovie1.FileName := FullVideoDir + VName;
//        ActiveMovie1.OpenSync;
        lastVideoName:=VName;
        AddList(vlcList01,FullVideoDir + VName,'');
      end
      else
      begin
//        ActiveMovie1.FileName := FullVideoDir + def_video;
//        ActiveMovie1.OpenSync;
        lastVideoName:=def_video;
        AddList(vlcList01,FullVideoDir + def_video,'');
      end;
    end
    else
    begin
        lastVideoName:=VName;
        AddList(vlcList01,VName,'URL');
//      ActiveMovie1.FileName := VName;
//      ActiveMovie1.OpenSync;
    end;
//    showdefaultstatus;
  except
    begin
//    ActiveMovie1.FileName := FullVideoDir + def_video;
//    ActiveMovie1.OpenSync;
      lastVideoName:=def_video;
      AddList(vlcList01,FullVideoDir + def_video,'');
    end;
  end;
end;

function TfrmVideo.FirstVideoAvail: boolean;
var
  bFoundVideo: boolean;
  sVideoFile:string;
begin
  RefreshPlayQ;

{
  if (not dm.tblPlayQ.IsEmpty) and (not dm.tblQF.IsEmpty) then
  begin
    dm.tblPlayQ.First;
    dm.tblQF.locate('id', dm.tblPlayQID.AsInteger, []);
    result := CheckFileExist(dm.tblQF.fieldbyname('newfilename').AsString);
  end
  else
  begin
    result := false;
  end;
}
  result:=false;
  if (not dm.tblPlayVideo.IsEmpty) then begin
    iMaxVideo:=dm.tblPlayVideo.RecordCount;
    dm.tblPlayVideo.First;
    bFoundVideo:=false;
    while (not dm.tblPlayVideo.Eof) and (not bFoundVideo) do begin
      sVideoFile:=dm.tblPlayVideo.fieldbyName('newFileName').AsString;
      bFoundVideo:=CheckFileExist(sVideoFile);
      dm.tblPlayVideo.Next;
    end;
    result:=bFoundVideo;
  end;
end;

function TfrmVideo.NextAvailVideo: string;
var
  NextIdx, PlayQCnt,iCount: integer;
  bFoundNextVideo: boolean;
  sVideoFile,sLog:string;
begin
  result := '';
  sVideoType:='';

  try
    RefreshPlayQ;
    bFoundNextVideo:=false;
    with dm.tblPlayVideo do begin
      PlayQCnt:=RecordCount;
      iMaxVideo:=PlayQCnt;
      First;
// Move to current Video Index
      iCount:=0;
      if (currVideoIdx+1 > PlayQCnt) then
        currVideoIdx:=0;

      while (iCount < currVideoIdx) do begin
        inc(iCount);
        next;
      end;

// Find Next Video
      while (Not EOF) and (not bFoundNextVideo) do begin
        inc(currVideoIdx);
        sVideoFile:=fieldbyName('newFileName').asString;
        sVideoType:=fieldbyName('filetype').AsString;

        if (lowercase(sVideoType)='url') then begin
          bFoundNextVideo:=true;
          sVideoFile:=fieldbyName('filelocation').AsString;
        end else begin
          bFoundNextVideo:= CheckFileExist(sVideoFile);
        end;
        if (not bFoundNextVideo) then begin
          sLog:='Q'+inttostr(iCurrQueue)+' ('+inttostr(currVideoIdx)
            +'/'+ inttostr(iMaxVideo) + ') '+sVideoFile + ' not found.';
          TrackLog('Video',sLog);
        end;
        Next;
      end;
      result:=sVideoFile;
      if (not bFoundNextVideo) then begin
        result:='';
      end;



      if (currVideoIdx >= PlayQCnt) then
        currVideoIdx:=0;
    end;

  except
    result := '';
  end;
end;

function TfrmVideo.AtLeastVideoAvail: boolean;
var
  sVideoFile:string;
begin
  result := false;

  try
    RefreshPlayQ;
    with dm.tblPlayVideo do begin
      if ( IsEmpty or EOF) then
        Exit;
      First;
      while (not Eof) and (not IsEmpty) do begin
        sVideoFile:=fieldbyname('newfilename').AsString;
        if (CheckFileExist(sVideoFile)) then begin
          result:=true;
          break;
          end;
        Next;
      end;
//    dm.tblPlayQ.First;
{
    while (not dm.tblPlayQ.Eof) and (not dm.tblQF.IsEmpty) do
    begin
      if dm.tblQF.locate('id', dm.tblPlayQID.AsInteger, []) then
      begin
        if CheckFileExist(dm.tblQF.fieldbyname('newfilename').asstring) then
          result := true;
      end;
      dm.tblPlayQ.Next;
    end;
}
    end;
  except
    result := false;
  end;
end;


function TfrmVideo.CheckFileExist(FName: string): boolean;
begin
  try
    result := FileExists(FullVideoDir + FName);
  except
    result := false;
  end;
end;


//--------------------------------------------------------------------------------
// Playing Banner
//--------------------------------------------------------------------------------
procedure TfrmVideo.TimerBTimer(Sender: TObject);
begin
  if Show_Ads then begin
    RefreshBanner;
    ShowBanners;
  end;
end;

procedure TfrmVideo.ShowBanners;
var
  sBanner1,sBanner2:string;
  sLog: string;
  iCountBanner1,iCountBanner2:integer;
begin
//  NextBannerName1 := NextAvailBanner1;
  NextBannerName1 := NextBanner1;
  sBanner1:= FullVideoDir + NextBannerName1;

  if (not fileexists(sBanner1)) then begin
    NextBannerName1:='ban01.jpg';
    sBanner1:=FullVideoDir + NextBannerName1;
  end;

  if CurrBannerName1 <> NextBannerName1 then begin
    Image01.Picture.LoadFromFile(sBanner1);
    CurrBannerName1 := NextBannerName1;
    sLog:='Banner1 :: Q'+inttostr(iCurrQueue)
      +' ('+ inttostr(CurrBannerIdx1)+'/'+ inttostr(iMaxBanner1)
      + ') '+ NextBannerName1;
    TrackLog('banner',sLog);
  end;

  if BannerMode = 2 then begin
//    NextBannerName2 := NextAvailBanner2;
    NextBannerName2 := NextBanner2;
    sBanner2:= FullVideoDir + NextBannerName2;
    if (not fileexists(sBanner2)) then begin
      NextBannerName2:='ban02.jpg';
      sBanner2:=FullVideoDir + NextBannerName2;
    end;
    if CurrBannerName2 <> NextBannerName2 then begin
      Image02.Picture.LoadFromFile(sBanner2);
      CurrBannerName2 := NextBannerName2;
      sLog:='Banner2 :: Q'+inttostr(iCurrQueue)
        +' ('+ inttostr(CurrBannerIdx2)+'/'+ inttostr(iMaxBanner2)
        + ') '+ NextBannerName2;
      TrackLog('banner',sLog);
    end else begin
      sLog:='Banner2 :: ('+ inttostr(CurrBannerIdx2+1)+'/'+ inttostr(iMaxBanner2)
        + ') :: '+ sBanner2;
      TrackLog('banner',sLog);
    end;
  end;

  //Label1.Caption := CurrBannerName1;
  //Label2.Caption := CurrBannerName2;
end;

function TfrmVideo.NextAvailBanner1: string;
var
  NextIdx, PlayQCnt,iCount: integer;
  bFoundNextBanner: boolean;
  sBannerFile,sLog: string;
begin
  result := '';
  sVideoType:='';

  try
    RefreshBanner;
    bFoundNextBanner:=false;
    with dm.tblPlayBanner1 do begin
      PlayQCnt:=RecordCount;
      iMaxBanner1:=PlayQCnt;
      First;
// Move to current Video Index
      iCount:=0;
      if (currBannerIdx1+1 > PlayQCnt) then
        currBannerIdx1:=0;

      while (iCount < currBannerIdx1) do begin
        inc(iCount);
        next;
      end;

// Find Next Video
      while (Not EOF) and (not bFoundNextBanner) do begin
        inc(currBannerIdx1);
        sBannerFile:=fieldbyName('newFileName').asString;
        bFoundNextBanner:= CheckFileExist(sBannerFile);
        if (not bFoundNextBanner) then begin
          sLog:='Banner 1 Q'+inttostr(iCurrQueue)+' ('+inttostr(currBannerIdx1)
            +'/'+ inttostr(iMaxBanner1) + ') '+sBannerFile + ' not found.';
          TrackLog('Banner',sLog);
        end;
        Next;
      end;
      result:=sBannerFile;
      if (not bFoundNextBanner) then begin
        result:='';
      end;



      if (currBannerIdx1 >= PlayQCnt) then
        currBannerIdx1:=0;
    end;

  except
    result := '';
  end;
end;

function TfrmVideo.NextAvailBanner2: string;
var
  NextIdx, PlayQCnt,iCount: integer;
  FoundNextBanner: boolean;
  sBannerFile: string;
begin
  result := '';

  try
    RefreshBanner;
{
    PlayQCnt := dm.tblPlayBq2.RecordCount;
    iMaxBanner2:=PlayQCnt;

    dm.tblPlayBq2.First;
    if (PlayQCnt > 0) and (not dm.tblBanner.IsEmpty) then
    begin

      if PlayQCnt = 1 then
      begin
        dm.tblBanner.locate('id',  dm.tblPlayBq2.Fields[0].AsInteger, []);
        result := dm.tblBanner.Fieldbyname('newfilename').AsString;
        FoundNextBanner := True;
      end
      else
      begin
        FoundNextBanner := False;
        NextIdx := (CurrBannerIdx2 + 1) mod PlayQCnt;

        while (NextIdx <> CurrBannerIdx2) and (not FoundNextBanner) do
        begin
          dm.FindPlayBQ2Idx(NextIdx);
          dm.tblBanner.locate('id',  dm.tblPlayBq2.Fields[0].AsInteger, []);

          if CheckFileExist(dm.tblBanner.fieldbyname('newfilename').AsString) then
          begin
            result := dm.tblBanner.Fieldbyname('newfilename').AsString;

            FoundNextBanner := True;
            CurrBannerIdx2 := NextIdx;
          end;

          NextIdx := (NextIdx + 1) mod PlayQCnt;
        end;

        // not FoundNextBanner then result := '';
      end;
    end;
}
    PlayQCnt := dm.tblPlayBanner2.RecordCount;
    iMaxBanner2:=PlayQCnt;
    dm.tblPlayBanner2.First;
    if (PlayQCnt > 0) then begin
      if (CurrBannerIdx2 >= PlayQCnt) then begin
        NextIdx:=0;
      end Else begin
        NextIdx := (CurrBannerIdx2 + 1) mod PlayQCnt;
      end;
      iCount:=0;
      while (iCount >= NextIdx) do begin
        inc(iCount);
        dm.tblPlayBanner2.Next;
      end;
      sBannerFile:=dm.tblPlayBanner2.FieldByName('newFileName').asstring;
      if (CheckFileExist(sBannerFile)) then
        result:=sBannerFile;
    end;
  except
    result := '';
  end;
end;

procedure TfrmVideo.ShowDefaultDevice;
var sDisplay: string;
  rKey: integer;
begin
  if Not AtLeastVideoAvail then
  begin
// No Video Queue //
    bDefaultPlayer:=true;
    case def_type of
    0 : // Default Video
      begin
      TrackLog('debug','play default video');
      tsCap32_show.Connected:=false;
      tscap32_show.Visible:=false;
      vlcPlay01.Visible:=true;
      if (lastVideoName <> def_video) then
        begin
        if (not vlcPlay01.IsPlay) then
          PlayVideo(def_video);
        end;
      end;
    1 : // Video-In
      begin
      TrackLog('debug','player stop');
      vlcPlay01.Visible:=false;
      vlcList01.Stop;
      vlcList01.Clear;
      TrackLog('debug','video in stop');
      tsCap32_show.Connected:=false;
      TrackLog('debug','video in start');
      tsCap32_show.Connected:=true;
      tscap32_show.Visible:=true;
      tsCap32_show.Align:=alclient;
      end;
    2 :// Streaming
      begin
      TrackLog('debug','play streaming '+def_file);
      bStreaming:=true;
      tsCap32_show.Connected:=false;
      tscap32_show.Visible:=false;
      vlcPlay01.Visible:=true;
      if (not vlcPlay01.IsPlay) then
        begin
        TrackLog('debug','play streaming '+def_file);
        vlcList01.Clear;
        vlcList01.Add(def_file);
        Application.ProcessMessages;
        vlcList01.Play;
        end;
      end;
    end;
  end
  Else
  begin
// Have Video Queue //
    if (bDefaultPlayer) then
    begin
      TrackLog('debug','video in stop');
      tsCap32_show.Connected:=false;
      tscap32_show.Visible:=false;
      vlcPlay01.Visible:=true;
    end;

    bDefaultPlayer:=false;
    if (def_type = 2) and (bstreaming) then // Streaming
      begin
      TrackLog('debug','streaming stop');
      vlcList01.Stop;
      vlcList01.Clear;
      bStreaming:=false;
      end;

    if (not vlcPlay01.IsPlay) then
    begin
      vlcList01.Stop;
      vlcList01.Clear;
//      bStreaming:=false;
      NextVideoName := NextAvailVideo;
      if NextVideoName <> '' then
        begin
        TrackLog('debug','play video '+NextVideoName);
        PlayVideo(NextVideoName);
        end;
    end
    Else
    begin
      TrackLog('debug','now playing video '+lastVideoName);
    end;
  end;
{
//  showdefaultstatus;
//  if (default_videoin and (not firstVideoAvail)) then
  if ((def_type=1) and (not firstVideoAvail)) then
// Play Defaut Video-In When No Queue , default_videoin
    begin
//    ShowCursor(true);
//    activemovie1.Stop;
//    activemovie1.Visible:=false;
    vlcPlay01.Visible:=false;
    vlcPlay01.Pause;
    vlcList01.Clear;
    tscap32_show.Connected:=true;
    tscap32_show.Visible:=true;
    end
  else
    begin
    if (not FirstVideoAvail) then
// No Video Queue
      begin
//      if (default_videoin) then
      if (def_type=1) then
        begin
//        ShowCursor(true);
        tscap32_show.Connected:=true;
        tscap32_show.Visible:=true;
//        activemovie1.Visible:=false;
        vlcPlay01.Visible:=false;
        vlcPlay01.Pause;
        vlcList01.Clear;
        end
      else
        begin
//        ShowCursor(false);
        tscap32_show.Connected:=false;
        tscap32_show.Visible:=false;
//        activemovie1.Visible:=true;
        vlcplay01.Visible:=true;
        if (not vlcplay01.IsPlay) then
          begin
          PlayVideo(def_video);
          end;
        end;
      end
    else
// Have Video Queue
      begin
//        if (not activemovie1.visible) then
//        if (not vlcPlay01.visible) then
        if (not vlcplay01.isPlay) then
          PlayVideo(dm.tblQF.fieldbyname('newfilename').AsString);
      end;
    end;
}


end;

procedure TfrmVideo.ShowDefaultStatus;
var sDisplay: string;
  rKey: integer;
begin
  sDisplay:='';
//      sDisplay:=sDisplay+'Video-In Show=>';
//      if (tscap32_Show.Visible) then sDisplay:=sDisplay+'True'
//      else sDisplay:=sDisplay+'False';
//
  sDisplay:=sDisplay+',FirstVideoAvail=>';
  if (FirstVideoAvail) then sDisplay:=sDisplay+'True'
  else sDisplay:=sDisplay+'False';
//
  sDisplay:=sDisplay+',AtLeastAvail=>';
  if (AtLeastVideoAvail) then sDisplay:=sDisplay+'True'
  else sDisplay:=sDisplay+'False';
//
  sDisplay:=sDisplay+',default_videoin=>';
  if (default_videoin) then sDisplay:=sDisplay+'True'
  else sDisplay:=sDisplay+'False';
//
//  sDisplay:=sDisplay+',Playstate=>';
//  sDisplay:=sDisplay+inttostr(activemovie1.PlayState);
//
//  sDisplay:=sDisplay+',activemovie1.visible=>';
//  if (activemovie1.Visible) then sDisplay:=sDisplay+'True'
  if (vlcPlay01.Visible) then sDisplay:=sDisplay+'True'
  else sDisplay:=sDisplay+'False';

{
  if (activemovie1.PlayState<>0) then
    sDisplay:=sDisplay+activemovie1.FileName;
}
  if (vlcPlay01.Showing) then
    sDisplay:=sDisplay+vlcList01.Get(0);

  Runningtext1.Caption:=sDisplay;
  runningtext1.Refresh;
end;

procedure TfrmVideo.vlcPlay01MediaPlayerEndReached(Sender: TObject);
var
  iIndex:integer;
  sLength:string;
begin
  if not IsExiting then begin
    sLength:=inttostr(vlcPlay01.Width)+':'+inttostr(vlcPlay01.Height);
    iIndex:=currVideoIdx;
    if (currVideoIdx = 0) then iIndex:=iMaxVideo;
    TrackLog('video','end video '+inttostr(iIndex));
//    TrackLog('video','last ratio is '+sLength);
    DBLogPlay(inttostr(iCurrQueue),'video',sCurrVideoFile,'Stop');
//    showdefaultstatus;
//    showDefaultDevice;
//    vlcPlay01.FreeInstance;
    if AtLeastVideoAvail then begin
//      vlcPlay01.Align:=alClient;
      NextVideoName:=NextAvailVideo;
      if NextVideoName <> '' then begin
        sLength:=inttostr(vlcPlay01.Width)+':'+inttostr(vlcPlay01.Height);
//        TrackLog('video','New ratio is '+sLength);
        if (lowercase(sVideoType)='url') then begin
          PlayStream(NextVideoName);
        end else begin
          PlayVideo(NextVideoName);
        end;
      end else begin
        showDefaultDevice;
      end;
    end;
  end;
end;

procedure TfrmVideo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  showCursor(true);
  ToClose;
end;

procedure TfrmVideo.AddList(vlcList: TPasLibVlcMediaList; strFile,sType: string);
var iCount,iIndex: integer;
  sLength:string;
  sLog:string;
  sFile:string;
begin
  sLength:=inttostr(vlcPlay01.Width)+':'+inttostr(vlcPlay01.Height);
  TrackLog('ratio','[ADD List] Last Video Ratio '+vlcPlay01.GetAspectRatio);
  inc(iLoopVideo);
  if (lowercase(sType)='url') then begin
   sCurrVideoFile:=strFile;
  end else begin
    if (not FileExists(strFile)) then abort;
   sCurrVideoFile:=ExtractFileName(strFile);
  end;
  vlcList.Add(strFile);
  iCount:=vlcList.Count();
  iIndex:=CurrVideoIdx;
  if (CurrVideoIdx=0) then iIndex:=iMaxVideo;
  if ( iCount > 0 ) then begin
//    vlcPlay01.SetAspectRatio('');
//    vlcPlay01.Align:=alClient;
//    TrackLog('video','play video '+strFile);
//    sLog:='Video :: ('+ inttostr(CurrVideoIdx)+'/'+ inttostr(iMaxVideo)
//      + ') :: '+ strFile;
    sLog:='Q'+inttostr(iCurrQueue)+' ('+inttostr(iIndex)+'/'
      + inttostr(iMaxVideo)  + ') '+  sCurrVideoFile;
    TrackLog('video',sLog);
//    LastVideoName:=strFile;
    DBLogPlay(inttostr(iCurrQueue),'video',sCurrVideoFile,'Start');
//    sLength:=inttostr(vlcPlay01.Width)+':'+inttostr(vlcPlay01.Height);
//    vlcList.Stop;
    vlcList.Pause;
    vlcPlay01.SetAspectRatio(sLength);
    TrackLog('ratio','[ADD List]#Pause Last Video Ratio '+vlcPlay01.GetAspectRatio);
    vlcList.Play;
//    vlcList.Play;
    vlcList.SetPlayModeLoop;
    vlcList.Player.SetAudioMute(false);
    if (bVideoRatio) then begin
      vlcPlay01.SetAspectRatio('');
      TrackLog('ratio','[ADD List] Play Video KeepRatio');
    end else begin
      vlcPlay01.SetAspectRatio(sLength);
      TrackLog('ratio','[ADD List] Play Video Ratio ::'+sLength);
    end;
  end;

end;

procedure TfrmVideo.UpdateScreen;
begin
  setscreen;
end;

procedure TfrmVideo.KeyHookHotKeyPressed(HotKey: Cardinal; Index: Word);
begin
  if HotKey = vk_f10 then
  begin
//    MessageDlg('Press Resize', mtError, [mbok], 0);
  Application.CreateForm(TfrmPlacement, frmPlacement);
  showCursor(true);
  frmPlacement.Show;
  end;

  if HotKey = vk_f11 then
  begin
    UpdateFullScreen();
  end;

end;

function TfrmVideo.CompactAndRepair: Boolean; {DB = Path to Access Database}
var
  v: OLEvariant;
begin
  Result := True;
  try
    v := CreateOLEObject('JRO.JetEngine');
    try
      V.CompactDatabase('Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;Data Source=db.mdb',
                        'Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;Data Source=dbx.mdb');

      DeleteFile('db.mdb');
      RenameFile('dbx.mdb', 'db.mdb');
    finally
      V := Unassigned;
    end;
  except
    Result := False;
  end;
end;

procedure TfrmVideo.CompactDB;
begin
  If dm.db.Connected then dm.db.Close;

  try
    if CompactAndRepair then
    begin
//      MessageDlg('Database has been compact successfully.', mtInformation, [mbOK], 0);
    end
    else
    begin
      MessageDlg('Database compacting error! Close DS and DS Agent before start compact.', mtInformation, [mbOK], 0);
//      Application.Terminate;
      Self.Close;
    end;
  finally
  end;
end;

procedure TfrmVideo.FormHide(Sender: TObject);
begin
  ShowCursor(False);
end;

procedure TfrmVideo.FormActivate(Sender: TObject);
begin
  ShowCursor(false);
end;

procedure TFrmVideo.TrackLog(sCategory: string; sValue:string);
var
  sName,sDir,sFile,sNew,sError : string;
  iRun: integer;
begin
  if (bTrackLog) then begin
    sFile:=sProgramDir+'log/'+sTrackLog+'_'
      +formatdatetime('yymmdd',now())+'_'+LowerCase(sCategory)+'.log';

    try
      if (not FileExists(sFile)) then begin
        AssignFile(myLogFile,sFile);
        ReWrite(myLogFile);
        CloseFile(myLogFile);
      end;
      iRun:=0;
      while ((FileInUse(sFile)) and (iRun < 100)) do begin
        sleep(random(100)+100);
        inc(iRun);
      end;
      if (not FileInUse(sFile)) then begin
        AssignFile(myLogFile,sFile);
        Append(myLogFile);
        WriteLn(myLogFile,formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# '+ sValue);
        CloseFile(myLogFile);
      end;
    Except
      on E : Exception do begin
        sError:='Exception on Function = TrackLog('+sCategory+','+sValue+') ';
        sError:=sError+#13#10+'Exception Class = '+E.ClassName;
        sError:=sError+#13#10+'Exception Message = '+E.Message;
        ShowMessage(sError);
      end;
    End;
  end;
end;

procedure TFrmVideo.SaveLog(sCategory: string; sValue:string);
var
  sName,sDir,sFile,sNew,sError : string;
  iRun: integer;
begin
  sFile:=sProgramDir+'log/'+sCategory+'.log';
  try
    if (not FileExists(sFile)) then begin
      AssignFile(myLogFile,sFile);
      ReWrite(myLogFile);
      CloseFile(myLogFile);
    end;
    iRun:=0;
    while ((FileInUse(sFile)) and (iRun < 100)) do begin
      sleep(random(100)+100);
      inc(iRun);
    end;

    if (not FileInUse(sFile)) then begin
      AssignFile(myLogFile,sFile);
      Append(myLogFile);
      WriteLn(myLogFile, sValue);
//    WriteLn(myLogFile,formatdatetime('yyyy-mm-dd hh:nn:ss',now)+'# '+ sValue);
      CloseFile(myLogFile);
    end;
  Except
    on E : Exception do begin
      sError:='Exception on Function = SaveLog('+sCategory+','+sValue+') ';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
end;

procedure TfrmVideo.timLogTimer(Sender: TObject);
begin
  frmVideo.BringToFront;
  SaveLog(sOnlineLog,formatdatetime('yyyy-mm-dd hh:nn:ss',now));
end;

procedure TfrmVideo.UpdateFullScreen();
var
  iniFile: TIniFile;
  iFullScreen:integer;
  sLog:string;
begin
  iFullScreen:=0;
  bFullScreen:=not bFullScreen;
  sLog:='Change Screen to Default';
  if (bFullScreen) then begin
    iFullScreen:=1;
    sLog:='Change Screen to Fullscreen';
  end;
  iniFile:=TiniFile.Create(sProgramDir+'\display.ini');
  try
    iniFile.WriteInteger('Placement','fullscreen',iFullScreen);
  finally
    iniFile.Free;
  end;
  TrackLog('Video',sLog);
  TrackLog('Debug',sLog);
  UpdateScreen();
end;

procedure TfrmVideo.DBLogPlay(sQueue,sType,sFile,sAction:string);
var sDate:string;
  iQueue:integer;
begin
  sDate:=formatdatetime('yyyy-mm-dd hh:nn:ss',now);
  iQueue:=0;
  if (lowercase(sType)='video') then iQueue:=iLoopVideo;
  if (lowercase(sType)='banner1') then iQueue:=iLoopBanner1;
  if (lowercase(sType)='banner2') then iQueue:=iLoopBanner2;
  with dm.tblLogPlay do begin
    try
      append;
      fieldbyname('log_id').AsInteger:=iQueue;
      fieldbyname('qid').AsString:=sQueue;
      fieldbyname('type').asstring:=lowercase(sType);
      fieldbyname('file').AsString:=sFile;
      fieldbyname('action').AsString:=sAction;
      fieldbyname('cdate').AsString:=sDate;
      post;
    except
      cancel;
    end;
  end;
end;

function TfrmVideo.CheckBannerQueue(myDB:TADOTable):boolean;
var
  sFileName,sFile:string;
begin
  result:=false;
  with myDB do begin
    First;
    if (EOF) then exit;
    while (not EOF) do begin
      sFileName:=fieldbyname('newfilename').AsString;
      sFile:=FullVideoDir+sFileName;
      if (FileExists(sFile)) then begin
        result:=true;
        exit;
      end;
      next;
    end;
  end;
end;

function TfrmVideo.NextBanner1():string;
var
  iCount,PlayQCnt:integer;
  sFileName,sFile,sLog:string;
begin
  result:='';
  try
    RefreshBanner;
    with dm.tblPlayBanner1 do begin
      PlayQCnt:=RecordCount;
      iMaxBanner1:=PlayQCnt;
      First;
// Move to current Video Index
      iCount:=0;
      if (currBannerIdx1+1 > PlayQCnt) then
        currBannerIdx1:=0;

      while (iCount < currBannerIdx1) do begin
        inc(iCount);
        next;
      end;

// Find Next Video
      while (Not EOF)do begin
        inc(currBannerIdx1);
        sFileName:=fieldbyName('newFileName').asString;
        sFile:=FullVideoDir+sFileName;
        if (FileExists(sFile)) then begin
          result:=sFileName;
          break;
        end;
        sLog:='Q'+inttostr(iCurrQueue)
          +' ('+inttostr(currBannerIdx1)+'/'+ inttostr(iMaxBanner1)
          + ') '+sFileName + ' not found.';
          TrackLog('Banner',sLog);
        Next;
      end;
      if (currBannerIdx1 > PlayQCnt) then begin
        result:='';
        currBannerIdx1:=0;
      end;
    end;
  except
  end;
end;

function TfrmVideo.NextBanner2():string;
var
  iCount,PlayQCnt:integer;
  sFileName,sFile,sLog:string;
begin
  result:='';
  try
    RefreshBanner;
    with dm.tblPlayBanner2 do begin
      PlayQCnt:=RecordCount;
      iMaxBanner2:=PlayQCnt;
      First;
// Move to current Video Index
      iCount:=0;
      if (currBannerIdx2+1 > PlayQCnt) then
        currBannerIdx2:=0;

      while (iCount < currBannerIdx2) do begin
        inc(iCount);
        next;
      end;

// Find Next Video
      while (Not EOF)do begin
        inc(currBannerIdx2);
        sFileName:=fieldbyName('newFileName').asString;
        sFile:=FullVideoDir+sFileName;
        if (FileExists(sFile)) then begin
          result:=sFileName;
          break;
        end;
        sLog:='Q'+inttostr(iCurrQueue)
          +' ('+inttostr(currBannerIdx2)+'/'+ inttostr(iMaxBanner2)
          + ') '+sFileName + ' not found.';
          TrackLog('Banner',sLog);
        Next;
      end;
      if (currBannerIdx2 > PlayQCnt) then begin
        result:='';
        currBannerIdx1:=0;
      end;
    end;
  except
  end;
end;

procedure TfrmVideo.PlayStream(sURL: string);
begin
  if (vlcPlay01.isPlay) then begin
    vlcList01.Stop;
  end;
  vlcList01.Clear;
  AddList(vlcList01,sURL,'URL');
end;


procedure TfrmVideo.timWebPageTimer(Sender: TObject);
var sURL,sError:string;
begin
  timWebPage.Enabled:=false;
  sURL:=nextHTML();
//  RefreshHTML();
//  inc(iRunHTML);
//  if (iRunHTML >= iMaxHTML) then iRunHTML:=0;
//  sURL:=arrURL[iRunHTML];
//  ShowMessage('Run '+inttostr(iRunHTML)+' URL '+sURL);
  if (iMaxHTML > 0) then begin
    LoadURL(Chrome,sURL);
    timWebPage.Interval:=iIntervalHTML*1000;
    timWebPage.Enabled:=true;
  end;
end;

procedure TfrmVideo.LoadURL(browser:Tchromium;sURL:string);
var oURL:OleVariant;
begin
  sError:='[LoadURL] '+inttostr(iRunHTML+1)+'/'+inttostr(iMaxHTML);
  sError:=sError+' Show URL : '+sURL;
  TrackLog('Browser',sError);
  try
    DBLogPlay(inttostr(iCurrQueue),'URL',sURL,'Load');
    browser.Browser.MainFrame.LoadUrl(sURL);
//    Application.ProcessMessages;
  Except
    on E : Exception do begin
      sError:='Exception on Function = LoadURL('+sURL+') ';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
end;

procedure TfrmVideo.WebBrowser1NavigateComplete2(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
  sError:='[WebBrowser1NavigateComplete2] '+inttostr(iRunHTML+1)+'/'+inttostr(iMaxHTML);
  sError:=sError+' Show URL : '+URL;
  TrackLog('Browser',sError);
end;

procedure TfrmVideo.ChromeLoadStart(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame);
begin
  if (browser <> nil) and (browser.GetWindowHandle = chrome.BrowserHandle) and ((frame = nil) or (frame.IsMain)) then
    FLoading := True;

end;

procedure TfrmVideo.ChromeLoadEnd(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame;
  httpStatusCode: Integer; out Result: Boolean);
begin
  if (browser <> nil) and (browser.GetWindowHandle = chrome.BrowserHandle) and ((frame = nil) or (frame.IsMain)) then
    FLoading := False;

end;

procedure TfrmVideo.SetBrowserPos(oVideo:TPasLibVlcPlayer;oBrowser:TChromium);
begin
    if (sHTMLDirection='top') then begin  // Top
//      with Chrome do begin
      with oBrowser do begin
        Left:=0;
        Top:=0;
        Height:=floor(pnlMovie.Height/2);
        Width:=pnlMovie.Width;
      end;
      with oVideo do begin
        Left:=0;
        Top:=floor(pnlMovie.Height/2)+1;
        Height:=floor(pnlMovie.Height/2);
        Width:=pnlMovie.Width;
      end;
    end else if (sHTMLDirection='bottom') then begin  // Bottom
      with oVideo do begin
        Left:=0;
        Top:=0;
        Height:=floor(pnlMovie.Height/2);
        Width:=pnlMovie.Width;
      end;
//      with Chrome do begin
      with oBrowser do begin
        Left:=0;
        Top:=floor(pnlMovie.Height/2)+1;
        Height:=floor(pnlMovie.Height/2);
        Width:=pnlMovie.Width;
      end;
    end else if (sHTMLDirection='left') then begin  // Left
//      with Chrome do begin
      with oBrowser do begin
        Left:=0;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=floor(pnlMovie.Width/2);
      end;
      with oVideo do begin
        Left:=floor(pnlMovie.Width/2)+1;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=floor(pnlMovie.Width/2);
      end;
    end else if (sHTMLDirection='right') then begin  // Right
      with oVideo do begin
        Left:=0;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=floor(pnlMovie.Width/2);
      end;
//      with Chrome do begin
      with oBrowser do begin
        Left:=floor(pnlMovie.Width/2)+1;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=floor(pnlMovie.Width/2);
      end;
    end else begin  // Else
      with oVideo do begin
        SendToBack;
        Visible:=false;
      end;
//      with Chrome do begin
      with oBrowser do begin
        Left:=0;
        Top:=0;
        Height:=pnlMovie.Height;
        Width:=pnlMovie.Width;
        SendToBack;
      end;
    end;
end;

procedure TfrmVideo.RefreshHTML();
var iRun:integer;
begin
  iMaxHTML:=0;
  try
  if (http_server='http://online.fdsthai.com/') then begin
    with dm.QRY do begin
      if active then close;
      dm.QRY.SQL.Text:='SELECT * FROM qryHTMLUrl';
      dm.QRY.Open;
      iMaxHTML:=dm.QRY.RecordCount;
    end;

    with dm.tblHTMLUrl do begin
      if active then close;
      open;
      setLength(arrURL,iMaxHTML+1);
      iRun:=0;
      First;
      if (iMaxHTML>0) then begin
        repeat
          arrURL[iRun]:=FieldbyName('URL').AsString;
          inc(iRun);
          Next;
        until (EOF);
      end;
    end;
  end else begin
    with dm.QRY do begin
      if active then close;
      dm.QRY.SQL.Text:='SELECT * FROM html_url WHERE qid=0 ORDER BY Ordering,id';
      dm.QRY.Open;
      iMaxHTML:=dm.QRY.RecordCount;
      setLength(arrURL,iMaxHTML+1);
      iRun:=0;
      First;
      if (iMaxHTML>0) then begin
        repeat
          arrURL[iRun]:=FieldbyName('URL').AsString;
          inc(iRun);
          Next;
        until (EOF);
      end;
    end;
  end;
  except
    on E : Exception do begin
      sError:='Exception on Function = RefreshHTML';
      sError:=sError+#13#10+'Exception Class = '+E.ClassName;
      sError:=sError+#13#10+'Exception Message = '+E.Message;
      ShowMessage(sError);
    end;
  end;
end;

function TfrmVideo.NextHTML():string;
begin
  result:='http://www.google.co.th';
  RefreshHTML();
  inc(iRunHTML);
  if (iRunHTML >= iMaxHTML) then iRunHTML:=0;
  result:=arrURL[iRunHTML];
//  ShowMessage('Run '+inttostr(iRunHTML)+' URL '+sURL);
end;

initialization
  CefCache := 'cache';
  CefRegisterCustomScheme('file', True, False, False);
  CefRegisterSchemeHandlerFactory('file', '', True, TFileScheme);


end.
