unit fmPlacement;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, INIfiles;

type
  TfrmPlacement = class(TForm)
    txtLeft: TEdit;
    txtTop: TEdit;
    txtWidth: TEdit;
    txtHeight: TEdit;
    cmdSave: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    chkFullscreen: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cmdSaveClick(Sender: TObject);
    procedure chkFullscreenClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormHide(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlacement: TfrmPlacement;

implementation

uses fmVideo;
{$R *.dfm}

procedure TfrmPlacement.FormActivate(Sender: TObject);
begin
  txtLeft.Text:=inttostr(frmVideo.Left);
  txtTop.Text:=inttostr(frmVideo.Top);
  txtWidth.Text:=inttostr(frmVideo.Width);
  txtHeight.Text:=inttostr(frmVideo.Height);
  showCursor(True);
end;

procedure TfrmPlacement.FormCreate(Sender: TObject);
begin
  showCursor(True);
  hide();
end;

procedure TfrmPlacement.cmdSaveClick(Sender: TObject);
var iniFile: TIniFile;
begin
    iniFile:=TiniFile.Create(GetCurrentDir+'\display.ini');
    try
      iniFile.WriteInteger('Placement','left',strtoint(txtLeft.Text));
      iniFile.WriteInteger('Placement','top',strtoint(txtTop.Text));
      iniFile.WriteInteger('Placement','width',strtoint(txtWidth.Text));
      iniFile.WriteInteger('Placement','height',strtoint(txtHeight.Text));
    finally
      iniFile.Free;
    end;

//  frmVideo.Width:=strtoint(txtWidth.Text);
//  frmVideo.Height:=strtoint(txtHeight.Text);
//  frmVideo.Left:=strtoint(txtLEft.Text);
//  frmVideo.Top:=strtoint(txtTop.Text);
  frmVideo.UpdateScreen();
  Self.Close;
end;

procedure TfrmPlacement.chkFullscreenClick(Sender: TObject);
begin
  if (self.chkFullscreen.Checked) then
  begin
    txtLeft.Text:='0';
    txtTop.Text:='0';
    txtWidth.Text:=inttostr(screen.Width);
    txtHeight.Text:=inttostr(screen.Height);
  end;
end;

procedure TfrmPlacement.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ShowCursor(false);
end;

procedure TfrmPlacement.FormHide(Sender: TObject);
begin
  ShowCursor(false);
end;
procedure TfrmPlacement.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (key=#27) then self.Close;
end;

end.
